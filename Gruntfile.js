module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {

            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                compress: true,
                mangle: true
            },
            build_app: {
                src: 'src/app.js',
                dest: 'build/app.min.js'
            },
            build_email: {
                src: 'src/email.js',
                dest: 'build/email.min.js'
            }



        },
        'string-replace': {
            inline: {
                files: {
                    'build/': 'build/app.min.js',
                },
                options: {
                    replacements: [
                        // place files inline example
                        {
                            pattern: "./email.js",
                            replacement: "./email.min.js"
                        }
                    ]

                }
            }
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-string-replace');

    // Default task(s).

    grunt.registerTask('default', ['uglify', 'string-replace'])

};