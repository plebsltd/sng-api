require('dotenv').config()
const Email = require('./email.js');
const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const bodyParser = require('body-parser');
const path = require('path');
const nodemailer = require('nodemailer');
const logger = require('simple-node-logger'),
    opts = {
        logFilePath: 'logfile.log',
        timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
    },
    log = logger.createSimpleLogger(opts);

const app = express();

const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: 587,
    secure: false,
    auth: {
        user: 'no-reply@supplyandgo.com',
        pass: 'administrator!'
    }
});

//  Create DB Pool

const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SCHEMA,
    multipleStatements: true,
});

//  API paths
const urlCustom = '/api/custom'
const urlOrders = '/api/orders'
const urlDistOrders = '/api/dist_orders'
const urlOrder = '/api/order'
const urlLatestOrderId = '/api/latest_order_id'
const urlOrderDist = '/api/order_dist'
const urlOrderProducts = '/api/order_products'
const urlOrderProductsByOrders = '/api/order_products_by_order'
const urlDistOrderProductsByOrders = '/api/dist_order_products_by_order'
const urlProducts = '/api/products'
const urlProduct = '/api/product'
const urlProductsByDist = '/api/products_by_dist'
const urlDistProducts = '/api/dist_products'
const urlDistributors = '/api/distributors'
const urlDistributor = '/api/distributor'
const urlLogin = '/api/login'
const urlEmail = '/api/send_email'
const urlULogin = '/api/ulogin'
const urlSettings = '/api/settings'
const urlUserDetails = '/api/user_details'
const urlDistUserDetails = '/api/dist_user_details'
const urlDashboard = '/api/dashboard'
const urlVerifyUser = '/api/verify_user'
const urlProductsCount = '/api/count_prod'
const urlTokenVerify = '/api/tokenverify'
const urlStock = '/api/test/stock'
const urlProductDeals = '/api/prod_deals'
const urlStockCh = '/api/stock_change'


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));


/*--------------------
----------------------
-----LOG MESSAGES-----
----------------------
--------------------*/

//  JWT Error Message
jwterr = (err) => {
    return `JWT | Message: ${err.message} | Expired at: ${err.expiredAt}`
}

//  SQL Connection Error Message
sqlconnerr = (err) => {
    return `SQL CONN | Message: ${err.message} | ADDRESS: ${err.address} | CODE:  ${err.code} | ERRNO: ${err.errno} | SYSCALL: ${err.syscall}`
}

//  Query Error Message
queryerr = (err, method, url) => {
    return `${method} | ${url} | Message: ${err.message} | SQL: ${err.sql} | SQL State: ${err.sqlState}`
}



//  Default Call
app.get('/api', (req, res) => {
    res.json({
        message: 'Welcome to the API'
    });
});

/*-------------------
---------------------
-----GET METHODS-----
---------------------
-------------------*/

//  GET Custom
// app.get(urlCustom, verifyToken, (req, res) => {
//     jwt.verify(req.token, 'secretkey', (err, authData) => {
//         if (err) {
//             log.error(jwterr(err))
//             res.sendStatus(403)
//         }
//         else {
//             log.info("JWT | Auth: ", authData);
//             pool.getConnection((err, connection) => {
//                 if (err) {
//                     log.fatal(sqlconnerr(err))
//                     res.json({ message: "There was an error connecting to the database" })
//                 }
//                 else {
//                     var query = req.headers.data

//                     connection.query(query, (err, results) => {
//                         if (err) {
//                             log.error(queryerr(err, "GET", urlCustom))
//                             res.json({ message: "There was an error fetching the data" })
//                         }
//                         else {
//                             log.info("GET | ", urlCustom, " | User: ", authData, " | Query: ", query)
//                             res.json({
//                                 query: query,
//                                 data: results,
//                                 authData,
//                                 iat: new Date(authData.iat * 1000).toLocaleTimeString(),
//                                 exp: new Date(authData.exp * 1000).toLocaleTimeString()
//                             })
//                         }
//                     })
//                     connection.release();
//                 }


//             })
//         }
//     })
// })

//  GET Token Verification
app.get(urlTokenVerify, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.json({
                message: "INVALID"
            })
        }
        else {
            res.json({
                message: "VALID"
            })
        }
    });
});

//  GET Product Deals
app.get(urlProductDeals, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    var query = `SELECT * FROM prod_deals`
                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlProductDeals))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlProductDeals, " | User: ", authData)


                            res.json({
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();

                }
            });
        }
    });
});


//  GET Count Products
// app.get(urlProductsCount, verifyToken, (req, res) => {
//     jwt.verify(req.token, 'secretkey', (err, authData) => {
//         if (err) {
//             log.error(jwterr(err))
//             res.sendStatus(403)
//         }
//         else {
//             log.info("JWT | Auth: ", authData);
//             pool.getConnection((err, connection) => {
//                 if (err) {
//                     log.fatal(sqlconnerr(err))
//                     res.json({ message: "There was an error connecting to the database" })
//                 }
//                 else {

//                     var query = `SELECT COUNT(*) as cp FROM products WHERE pDID=${authData.userID}`
//                     connection.query(query, (err, results) => {

//                         if (err) {
//                             log.error(queryerr(err, "GET", urlProductsCount))
//                             res.json({ message: "There was an error fetching the data" })
//                         }
//                         else {
//                             log.info("GET | ", urlProductsCount, " | User: ", authData)


//                             res.json({
//                                 authData,
//                                 data: results,
//                                 iat: new Date(authData.iat * 1000).toLocaleTimeString(),
//                                 exp: new Date(authData.exp * 1000).toLocaleTimeString()
//                             });
//                         }
//                     });
//                     connection.release();

//                 }
//             });
//         }
//     });
// });

//  GET Dashboard Data
app.get(urlDashboard, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    var query = `select pStock, opPID as id, pname as product, pprice as price, pdesc as description, pvat as vat, sum(opQuant) as total, udAddress, udEmail, udID, uFname, udPhone, pDID, pDesc, pID, pName, pPrice, pVAT from order_products inner join order_history on (opOHID=ohID) inner join products on (opPID=pID) inner join user_details on (pDID=udID)  where ohStatus='CONFIRMED' and ohUID=${authData.userID} group by opPID order by total desc limit 5; select udID, uFname as distributor, udEmail as email, sum(ohTotal) as total , date_format(min(ohTimestamp),'%D %M %Y') as first_order, date_format(max(ohTimestamp),'%D %M %Y') as last_order from order_history inner join user_details ON (udID=ohdid) where ohStatus = 'CONFIRMED' and ohuid=${authData.userID} group by ohdid order by total desc limit 5; select year(ohTimestamp) as year, sum(ohTotal) as total from order_history where ohUID=${authData.userID} AND ohStatus='CONFIRMED' GROUP BY year ORDER BY year; select month(ohTimestamp) as month_value, { fn MONTHNAME(ohTimestamp)} as month, year(ohTimestamp) as year, sum(ohTotal) as total from order_history where ohUID=${authData.userID} AND ohStatus='CONFIRMED' GROUP BY year, month, month_value ORDER BY year, month_value ASC; select DATE_FORMAT(ohTimestamp,'%d/%m/%Y') as date,SUM(ohTotal) as money from order_history where ohUID=${authData.userID} AND ohStatus = 'CONFIRMED' AND DATEDIFF(now(),ohTimestamp)<7 GROUP BY date; select DATE_FORMAT(ohTimestamp,'%d/%m/%Y') as date, SUM(opQuant) as products from order_history INNER JOIN order_products ON (opOHID = ohID)  where ohUID=${authData.userID} AND ohStatus = 'CONFIRMED'  AND DATEDIFF(now(),ohTimestamp)<7 GROUP BY date`
                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlDashboard))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDashboard, " | User: ", authData)

                            var data = { topProd: results[0], topDist: results[1], yearData: results[2], monthData: results[3], weekData: results[4], weekProdData: results[5] }
                            res.json({
                                authData,
                                data: data,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();

                    // res.json(data)
                }
            });
        }
    });
});

//  GET Verify User
app.get(urlVerifyUser, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM users WHERE uID =${authData.userID} AND ?`

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlVerifyUser))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlVerifyUser, " | User: ", authData, " | Data: ", `ohUID=${authData.userID}`)
                            res.json({
                                // message: 'Orders for user '+data.ohUID,
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Order History
app.get(urlOrders, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    //var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_history INNER JOIN user_details ON (ohDID = udID) WHERE ohUID=${authData.userID} ORDER BY ohTimeStamp DESC`

                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlOrders))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlOrders, " | User: ", authData, " | Data: ", `ohUID=${authData.userID}`)
                            res.json({
                                // message: 'Orders for user '+data.ohUID,
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Order History
app.get(urlDistOrders, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    //var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_history INNER JOIN user_details ON (ohUID = udID) WHERE ohDID=${authData.userID} ORDER BY ohTimeStamp DESC`

                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlDistOrders))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDistOrders, " | User: ", authData, " | Data: ", `ohDID=${authData.userID}`)
                            res.json({
                                // message: 'Orders for user '+data.ohUID,
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

// GET Order
app.get(urlOrder, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_history INNER JOIN user_details ON (ohDID = udID) WHERE ?`

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlOrder))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlOrder, " | User: ", authData, " | Data: ", `ohUID=${authData.userID}`)
                            res.json({
                                // message: 'Orders for user '+data.ohUID,
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Latest Order ID 
app.get(urlLatestOrderId, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT ohID FROM order_history where ohUID = ${authData.userID} and ? ORDER BY ohID DESC LIMIT 1;`

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlLatestOrderId))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlLatestOrderId, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Letest Order ID',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

// GET Order Dist
app.get(urlOrderDist, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_history INNER JOIN user_details ON (ohDID = udID) WHERE ?`

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlOrderDist))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlOrderDist, " | User: ", authData, " | Data: ", `ohUID=${authData.userID}`)
                            res.json({
                                // message: 'Orders for user '+data.ohUID,
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});



//  GET Order Products by Order
app.get(urlOrderProductsByOrders, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_products as op INNER JOIN order_history as oh ON (op.opOHID= oh.ohID and oh.ohUID=${authData.userID}) INNER JOIN products as p ON (op.opPID = p.pID) INNER JOIN user_details as d ON (p.pDID = d.udID) LEFT OUTER JOIN prod_deals as pd ON (op.opPDID = pd.pdID ) WHERE ?`;

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlOrderProductsByOrders))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlOrderProductsByOrders, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Order Products by Order ID',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Dist Order Products by Order
app.get(urlDistOrderProductsByOrders, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM order_products as op INNER JOIN order_history as oh ON (op.opOHID= oh.ohID) INNER JOIN products as p ON (op.opPID = p.pID) INNER JOIN user_details as d ON (p.pDID = d.udID) WHERE ?`;

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlDistOrderProductsByOrders))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDistOrderProductsByOrders, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Order Products by Order ID',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Products
app.get(urlProducts, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var query = "SELECT * FROM products INNER JOIN user_details ON (pDID = udID)"

                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlProducts))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlProducts, " | User: ", authData)
                            res.json({
                                message: 'All Products',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Product
app.get(urlProduct, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    var query = "SELECT * FROM products WHERE ? "

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlProduct))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlProduct, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Product by productID ',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Products by Distributor
app.get(urlProductsByDist, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var data = JSON.parse(req.headers.data)

                    // var query = "SELECT * FROM products INNER JOIN distributors ON (pDID = dID) WHERE ?"
                    var query = "SELECT * FROM products INNER JOIN user_details ON (pDID = udID) WHERE ?"

                    connection.query(query, data, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlProductsByDist))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlProductsByDist, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Products by DistributorID ',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});



//  GET Distributor Products
app.get(urlDistProducts, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    // var data = JSON.parse(req.headers.data)

                    // var query = "SELECT * FROM products INNER JOIN distributors ON (pDID = dID) WHERE ?"
                    var query = `SELECT * FROM products INNER JOIN user_details ON (pDID = udID) WHERE pDID=${authData.userID}`

                    connection.query(query, (err, results) => {

                        if (err) {
                            log.error(queryerr(err, "GET", urlDistProducts))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDistProducts, " | User: ", authData)
                            res.json({
                                message: 'Products by DistributorID ',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});


//  GET Distributors
app.get(urlDistributors, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    var query = "SELECT udID, uFname, udEmail, udVatNo, udAddress, udPhone, udIcon FROM user_details INNER JOIN users ON (udID=uID) WHERE uIsDist = 1"

                    connection.query(query, (err, results) => {
                        if (err) {
                            log.error(queryerr(err, "GET", urlDistributors))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDistributors, " | User: ", authData)
                            res.json({
                                message: 'All Distributors',
                                authData,
                                data: results,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  Get Stock
app.get(urlStock, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            //  Get connection
            pool.getConnection((err, connection) => {
                //  Check for error
                if (err) {
                    res.json(err)
                }
                else {

                    //  SQL Query
                    var query = `SELECT stPID, pName, stSN, stTimestamp, pDesc, pPack, pVolume, uFname FROM stock INNER JOIN products ON (stPID=pID) INNER JOIN user_details ON (pDID = udID) WHERE stUID=${authData.userID}`

                    connection.query(query, (error, results) => {

                        //  Check for error and reject or resolve result
                        if (error) {
                            log.errorLog(error)
                            res.json(error)
                        }
                        else {
                            log.info("GET | ", urlStock)
                            res.json({
                                message: 'Stock by user',
                                data: results,
                                rowCount: results.length
                            })

                        }
                    });

                    //  Release connection
                    connection.release();
                }
            })
        }
    });
});



//  Get single Distributor
app.get(urlDistributor, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    var data = JSON.parse(req.headers.data)

                    var query = "SELECT udID, uFname, udEmail, udVatNo, udAddress, udPhone, udIcon FROM user_details INNER JOIN users ON(udID=uID) WHERE uIsDist=1 AND ?"

                    connection.query(query, data, (err, results) => {
                        if (err) {
                            log.error(queryerr(err, "GET", urlDistributor))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlDistributor, " | User: ", authData, " | Data: ", data)
                            res.json({
                                message: 'Distributor by DistributorID',
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString(),
                                data: results
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

var userID = [];

//  GET User and Login
app.get(urlLogin, (req, res) => {


    pool.getConnection((err, connection) => {
        if (err) {
            log.fatal(sqlconnerr(err))
            res.json({ message: "There was an error connecting to the database" })
        }
        else {
            // Get email and password from request
            var pass = md5(req.headers.email);
            var query = "SELECT * FROM api_users WHERE email=" + mysql.escape(req.headers.email) + " AND md5=" + mysql.escape(pass);

            connection.query(query, (err, results) => {

                if (err) {
                    log.error("GET | Message: ", err.message, ' | SQL: ', err.sql, " | SQL State: ", err.sqlState)
                    return res.send(err);
                }
                else {
                    if (results.length < 1) {
                        log.warn("GET | Invalid Login ", "| Query: ", query)
                        return res.sendStatus(400)
                    }
                    else {
                        userID = results[0].id;
                        firstname = results[0].first_name;
                        lastname = results[0].last_name;
                        jwt.sign({ userID }, 'secretkey', { expiresIn: '1000000000s' }, (err, token) => {
                            if (err) {
                                log.error("JWT | Message: ", err.message)
                                res.json({
                                    message: "Invalid JWT"
                                })
                            }
                            else {
                                log.info("GET | Login Request | User: ", results, " | Token given: ", token)

                                return res.json(
                                    {
                                        userID: results[0].id,
                                        email: results[0].email,
                                        name: firstname + ' ' + lastname,
                                        token: token
                                    }
                                );

                            }
                        });
                    }
                }
            });
            connection.release();
        }
    });

});

//  GET User Details
app.get(urlUserDetails, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    // var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM user_details WHERE udID=${authData.userID}`;

                    connection.query(query, (err, results) => {
                        if (err) {
                            log.error(queryerr(err, "GET", urlUserDetails))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlUserDetails, " | User: ", authData)
                            res.json({
                                message: 'User Details',
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString(),
                                data: results
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});

//  GET Dist User Details
app.get(urlDistUserDetails, verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    var data = JSON.parse(req.headers.data)

                    var query = `SELECT * FROM user_details WHERE ?`;

                    connection.query(query, data, (err, results) => {
                        if (err) {
                            log.error(queryerr(err, "GET", urlUserDetails))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("GET | ", urlUserDetails, " | User: ", authData)
                            res.json({
                                message: 'User Details',
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString(),
                                data: results
                            });
                        }
                    });
                    connection.release();
                }
            });
        }
    });
});



//  GET Login
app.get(urlULogin, (req, res) => {


    pool.getConnection((err, connection) => {
        if (err) {
            log.fatal(sqlconnerr(err))
            res.json({ message: "There was an error connecting to the database" })
        }
        else {
            // Get email and password from request
            var pass = ""
            // Get email and password from request
            if (req.headers.email !== undefined) {
                pass = md5(req.headers.email);
            }
            else {
                pass = req.headers.email
            }
            var query = "SELECT * FROM users INNER JOIN user_details ON (uID = udID) WHERE uID = ( SELECT id FROM api_users WHERE email=" + mysql.escape(req.headers.email) + " AND md5=" + mysql.escape(pass) + ") and uPass = MD5(" + mysql.escape(req.headers.password) + ")";

            connection.query(query, (err, results) => {

                if (err) {
                    log.error("GET | Message: ", err.message, ' | SQL: ', err.sql, " | SQL State: ", err.sqlState)
                    return res.send(err);
                }
                else {
                    if (results.length < 1) {
                        log.warn("GET | Invalid Login ", "| Query: ", query)
                        return res.sendStatus(400)
                    }
                    else {
                        userID = results[0].uID;
                        uFname = results[0].uFname;
                        var isForTesting = results[0].uIsForTesting
                        var userType = results[0].uIsDist

                        jwt.sign({ userID, uFname, isForTesting }, 'secretkey', { expiresIn: '10000000s' }, (err, token) => {
                            if (err) {
                                log.error("JWT | Message: ", err.message)
                                res.json({
                                    message: "Invalid JWT"
                                })
                            }
                            else {
                                log.info("GET | Login Request | User: ", userID, " | Token given: ", token)

                                return res.json(
                                    {
                                        userID: userID,
                                        name: uFname,
                                        userType: userType,
                                        token: token
                                    }
                                );

                            }
                        });
                    }
                }
            });
            connection.release();
        }
    });

});

/*------------------
--------------------
----POST METHODS----
--------------------
------------------*/


//  POST Custom
// app.post(urlCustom, verifyToken, (req, res) => {
//     jwt.verify(req.token, 'secretkey', (err, authData) => {
//         if (err) {
//             log.error(jwterr(err))
//             res.sendStatus(403)
//         }
//         else {
//             var query = req.headers.data;

//             log.info("JWT | Auth: ", authData);
//             pool.getConnection((err, connection) => {
//                 if (err) {
//                     log.fatal(sqlconnerr(err))
//                     res.json({ message: "There was an error connecting to the database" })
//                 }
//                 else {
//                     connection.query(query, (err, results, fields) => {
//                         if (err) {
//                             log.error(queryerr(err, "POST", urlCustom))
//                             res.json({ message: "There was an error fetching the data" })
//                         }

//                         else {
//                             log.info("POST | ", urlCustom, " | User: ", authData, " | Query: ", query)
//                             res.send({
//                                 message: results,
//                                 authData,
//                                 iat: new Date(authData.iat * 1000).toLocaleTimeString(),
//                                 exp: new Date(authData.exp * 1000).toLocaleTimeString()
//                             });

//                         }
//                     });
//                     connection.release();
//                 }
//             })
//         }

//     })
// })



//  POST Signup Page
app.post('/api/signup', (req, res) => {
    var query = ''
    let data = req.body

    query = `INSERT INTO signups (snName, snSurname, snCompany, snEmail, snPhone) VALUES (${data.snName},${data.snSurname},${data.snCompany},${data.snEmail},${data.snPhone})`;

    pool.getConnection((err, connection) => {
        if (err) {
            log.fatal(sqlconnerr(err))
            res.json({ message: "There was an error connecting to the database" })
        }
        else {
            connection.query(query, (err, results, fields) => {
                if (err) {
                    log.error(queryerr(err, "POST", '/api/signup'))
                    res.json({ message: "There was an error posting the data" })
                }
                else {
                    log.info("POST | ", '/api/signup', "| Data: ", data)
                    res.send({
                        message: results,
                        success: true
                    });
                }
            })
            connection.release();
        }
    })
})

//  POST Products
app.post(urlProducts, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {

            var query = ''
            let data = JSON.parse(req.headers.data)
            data.forEach((e) => {
                query += mysql.format("INSERT INTO products (pDID, pName, pPrice, pDesc, pCategory, pSubcategory, pPack, pVolume, pVat, pUnit) VALUES (?,?,?,?,?,?,?,?,?,?); ", e)
            })

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, (err, results) => {
                        if (err) {
                            log.error(queryerr(err, "POST", urlProducts))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("POST | ", urlProducts, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});

//  POST Order Products
app.post(urlOrderProducts, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            //let query = "INSERT INTO order_products VALUES ?"
            var query = ''
            let data = JSON.parse(req.headers.data)
            data.forEach((e) => {
                query += mysql.format("INSERT INTO order_products (opOHID, opPID, opQuant, opDisc, opPrice, opName, opDesc, opVat, opPDID, opSubtotal) VALUES (?,?,?,?,?,?,?,?,?,?); ", e)
            })

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "POST", urlOrderProducts))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("POST | ", urlOrderProducts, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});


// POST Order History
app.post(urlOrders, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = `INSERT INTO order_history (ohID, ohTotal, ohTimestamp, ohUID, ohDID) VALUES (?, ?, ?, ${authData.userID}, ?)`

            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "POST", urlOrders))

                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("POST | ", urlOrders, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                fields: fields,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});

//  POST Email
app.post(urlEmail, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {

                    if (data.sendingOrder) {

                        let query = `SELECT ohID FROM order_history WHERE ohUID = ${authData.userID} AND ohDID = ${data.order.pDID} ORDER BY ohTimestamp DESC LIMIT 1`;

                        connection.query(query, data, (err, results) => {

                            if (err) {
                                log.error(queryerr(err, "GET", urlEmail))
                                res.json({ message: "There was an error fetching the data" })
                            }
                            else {
                                log.info("GET | Latest OrderID | User: ", authData.userID, " | Data: ", data, " | Results: ", results)
                                res.json({
                                    message: 'Order ID:',
                                    authData,
                                    data: results,
                                    iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                    exp: new Date(authData.exp * 1000).toLocaleTimeString()
                                });

                                log.info("JWT | Auth: ", authData);

                                var to = data.distEmail
                                if (authData.isForTesting) {
                                    to = "info@supplyandgo.com"
                                }

                                var mailOptions = {
                                    // from: 'no-reply@supplyandgo.com',
                                    to: to,
                                    //to: 'info@supplyandgo.com',
                                    subject: Email.createSubject(data, results, authData.uFname),
                                    text: Email.createBodyText(data, results, authData.uFname),
                                    headers: {
                                        priority: 'high'
                                    }
                                };

                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        log.error("EMAIL | Message:", error)
                                        res.send("Error in sending email")
                                    } else {
                                        log.info("EMAIL | ", info.response)
                                        res.send("Email Sent")
                                    }
                                });
                            }

                        })
                        connection.release();
                    }
                    else {
                        var mailOptions = {
                            from: 'testemailpleb@gmail.com',
                            //to: ${data.distEmail}
                            to: 'testemailpleb@gmail.com',
                            subject: Email.createSubject(data, null, authData.uFname),
                            text: Email.createBodyText(data, null, authData.uFname),
                            headers: {
                                priority: 'high'
                            }
                        };

                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                log.error("EMAIL | Message:", error)
                                res.send("Error in sending email")
                            } else {
                                log.info("EMAIL | ", info.response)
                                res.send("Email Sent")
                            }
                        });
                    }
                }
            });



        }
    });
})


/*-----------------
-------------------
----PUT METHODS----
-------------------
-----------------*/

//  PUT Custom
// app.put(urlCustom, verifyToken, (req, res) => {
//     jwt.verify(req.token, 'secretkey', (err, authData) => {
//         if (err) {
//             log.error(jwterr(err))
//             res.sendStatus(403)
//         }
//         else {
//             var query = req.headers.data;

//             log.info("JWT | Auth: ", authData);
//             pool.getConnection((err, connection) => {
//                 if (err) {
//                     log.fatal(sqlconnerr(err))
//                     res.json({ message: "There was an error connecting to the database" })
//                 }
//                 else {
//                     connection.query(query, (err, results, fields) => {
//                         if (err) {
//                             log.error(queryerr(err, "PUT", urlCustom))
//                             res.json({ message: "There was an error fetching the data" })
//                         }
//                         else {
//                             log.info("PUT | ", urlCustom, " | User: ", authData, " | Query: ", query)
//                             res.send({
//                                 message: results,
//                                 fields: fields,
//                                 authData,
//                                 iat: new Date(authData.iat * 1000).toLocaleTimeString(),
//                                 exp: new Date(authData.exp * 1000).toLocaleTimeString()
//                             });
//                         }
//                     });
//                     connection.release();
//                 }
//             })
//         }

//     })
// })

//  PUT Order History
app.put(urlOrders, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = "UPDATE order_history SET ? WHERE ?;"

            let data = JSON.parse(req.headers.data)
            if (data[0].ohStatus == "CANCELED") {
                data[2].forEach((e) => {
                    query += `UPDATE stock SET stSN = stSN - ${e.opQuant} WHERE stPID=${e.opPID} and stUID=${e.ohUID};`
                    query += `UPDATE stock SET stSN = 0 WHERE stSN < 0 and stPID=${e.opPID} and stUID=${e.ohUID};`
                })

            }

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlOrders))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlOrders, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});

//  PUT Order Products
app.put(urlOrderProducts, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = ""

            let data = JSON.parse(req.headers.data)

            data.forEach((e) => {
                query += mysql.format("UPDATE order_products SET opDisc = ? WHERE opOHID = ? AND opPID = ?; ", e)
            })

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlOrderProducts))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlOrderProducts, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});

//  PUT Product
app.put(urlProduct, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = "UPDATE products SET ? WHERE ?"

            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlProduct))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlProduct, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});


//  PUT User Settings
app.put(urlSettings, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = `UPDATE users SET ? WHERE uID=${authData.userID}`

            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlSettings))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlSettings, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});


//  PUT user_details
app.put(urlUserDetails, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = `UPDATE user_details SET ? WHERE udID=${authData.userID}`

            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlUserDetails))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlUserDetails, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});


//  PUT Stock
app.put(urlStockCh, verifyToken, (req, res) => {


    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            log.error(jwterr(err))
            res.sendStatus(403)
        }
        else {
            let query = "UPDATE stock SET ? WHERE ?"

            let data = JSON.parse(req.headers.data)

            log.info("JWT | Auth: ", authData);
            pool.getConnection((err, connection) => {
                if (err) {
                    log.fatal(sqlconnerr(err))
                    res.json({ message: "There was an error connecting to the database" })
                }
                else {
                    connection.query(query, data, (err, results, fields) => {
                        if (err) {
                            log.error(queryerr(err, "PUT", urlStockCh))
                            res.json({ message: "There was an error fetching the data" })
                        }
                        else {
                            log.info("PUT | ", urlStockCh, " | User: ", authData, " | Data: ", data)
                            res.send({
                                message: results,
                                authData,
                                iat: new Date(authData.iat * 1000).toLocaleTimeString(),
                                exp: new Date(authData.exp * 1000).toLocaleTimeString()
                            });
                        }
                    })
                    connection.release();
                }
            })
        }
    })
});




// FORMAT OF TOKEN
// Authorization: Bearer <access_token>

// Verify Token
function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        next();
    } else {
        // Forbidden
        res.sendStatus(403);
    }

}

app.listen(process.env.PORT || 5000, () => console.log('Server started on port 5000'));