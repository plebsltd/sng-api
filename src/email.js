module.exports = {
    createBodyText : function(data, results, uFname) {
        let emailText = '';
        let orderArray = [];
        let price = 0;
        let dist = '';
        let user = uFname.toUpperCase();
        let footer = `We hope to see you again soon!\n\nSupplyAndGo Team!`;
        let contactInfo = `info@supplyandgo.com`;
        if (data.sendingOrder) {
            dist = data.order.uFname.toUpperCase();
            emailText = `Dear ${dist} we would like to inform you that a new order has been placed from ${user}`;
            emailText += `\n\n---------------------------------------\nOrder for distributor ${dist} ORDER #: ${results[0].ohID}`;
            emailText += `\n---------------------------------------\n`;
            emailText += `\nBelow you can find the order's items.\n\nPRODUCTS:\n`;
            orderArray = data.order.data;
    
            for (var order in orderArray) {
                emailText += `ID: ${orderArray[order].pID}, Name: ${orderArray[order].pName}, Desc: ${orderArray[order].pDesc}, Price:€ ${orderArray[order].pPrice.toFixed(2)} ,Quantity: ${orderArray[order].quantity} ,Sub-Total:€ ${(orderArray[order].quantity * orderArray[order].pPrice).toFixed(2)}\n`
                price += (orderArray[order].quantity * orderArray[order].pPrice);
            }
    
            emailText += `\n\nThe total price of the order amounts to: € ${price.toFixed(2)}\nVAT No: ${data.details.udVatNo}\n`;
            emailText += `The above prices do not include VAT!\n`
            emailText += `-----------------------------------------------------`
            emailText += `\n\nIf you have any inquiries or need further assistance with the order, please contact SupplyAndGo team at ${contactInfo}!`;
            emailText += `\nThank you for using SupplyAndGo!`
            emailText += `\nWe hope to see you again soon!`;
            emailText += `\n\n\nPlease note: This email was sent from a notification-only address that can't accept incoming email. Please do not reply to this message.`;
            emailText += `${footer}`;
    
            return emailText;
        }
        else {
            orderArray = data.order;
            dist = orderArray[0].uFname.toUpperCase();
            emailText += `Dear ${dist} we would like to inform you that the ORDER #: ${orderArray[0].ohID} you have received from ${uFname.toUpperCase()} has been canceled!\n`;
            emailText += `\nThe items listed below were part of the canceled order!\n`;
            emailText += `\n---------------------------------------------------------\n`;
    
            for (var order in orderArray) {
                emailText += `ID: ${orderArray[order].pID}, Name: ${orderArray[order].pName}, Desc: ${orderArray[order].pDesc}, Price:€ ${orderArray[order].pPrice.toFixed(2)} ,Quantity: ${orderArray[order].opQuant} ,Sub-Total:€ ${(orderArray[order].opQuant * orderArray[order].pPrice).toFixed(2)}\n`;
                price += (orderArray[order].opQuant * orderArray[order].pPrice);
            }
    
            emailText += `\nThe total amount of the order amounts to € ${price}`;
            emailText += `\nVAT No: 10317322\nThe prices above do not include VAT!`;
            emailText += `\n----------------------------------------------------------\n`;
            emailText += `\nIf you have any inquiries please contact SupplyAndGo team at ${contactInfo}! `;
            emailText += `\n\n\nPlease note: This email was sent from a notification-only address that can't accept incoming email. Please do not reply to this message.`;
            emailText += `${footer}`;
        }
        return emailText;
    },
    
    createSubject : function(data, results, uFname) {
        let subject = '';
        let user = uFname.toUpperCase();
        if (data.sendingOrder) {
            if(results === undefined){
                results.push({ohID:1})
            }
            let orderID = results[0].ohID;
            subject += `New Order from ${uFname.toUpperCase()} OrderID: ${orderID}`;
        }
        else {
            let orderID = data.order[0].ohID;
            subject += `ORDER CANCELATION NOTIFICATION: ORDER # ${orderID} From: ${user}`;
        }
        return subject;
    }
}